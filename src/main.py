# -*- coding: utf-8 -*-

# Nicolas, 2021-03-05
from __future__ import absolute_import, print_function, unicode_literals
from copy import copy

import random
from turtle import pos 
import numpy as np
import sys
from itertools import chain

import pygame


from pySpriteWorld.gameclass import Game,check_init_game_done
from pySpriteWorld.spritebuilder import SpriteBuilder
from pySpriteWorld.players import Player
from pySpriteWorld.sprite import MovingSprite
from pySpriteWorld.ontology import Ontology
import pySpriteWorld.glo

from search.grid2D import ProblemeGrid2D
from search import probleme

import strat




# ---- ---- ---- ---- ---- ----
# ---- Misc                ----
# ---- ---- ---- ---- ---- ----




# ---- ---- ---- ---- ---- ----
# ---- Main                ----
# ---- ---- ---- ---- ---- ----

game = Game()

def init(_boardname=None):
    global player,game
    name = _boardname if _boardname is not None else 'blottoMap'
    game = Game('./Cartes/' + name + '.json', SpriteBuilder)
    game.O = Ontology(True, 'SpriteSheet-32x32/tiny_spritesheet_ontology.csv')
    game.populate_sprite_names(game.O)
    game.fps = 120  # frames per second
    game.mainiteration()
    player = game.player

def aEtoile(j,path,i,posPlayers,players,objectifs):
    if len(path[j])>i:
        row,col = path[j][i]
        posPlayers[j]=(row,col)
        #players[j%len(players)].set_rowcol(row,col)
        #print (f"pos j{j%len(players)+1} e{j//len(players)+1}:", row,col)
        if (row,col) == objectifs[j%len(players)]:
            print(f"le joueur {j%len(players)+1}, equipe {j//len(players)+1} a atteint son but!")
    else:
        return 1
    return 0

def main():
    nbJour = int(input("Nombre de jours : "))

    print("Budget de déplacement :\n\t1 : illimité\n\t2 : par jour\n\t3 : pour la campagne entière")
    match (int(input('Choix du type de budget : '))-1)%3:
        case 0:
            Budget = 10000
            BudgetPartiInit = [10000*nbJour]*2
        case 1:
            Budget = int(input('Valeur du budget : '))
            BudgetPartiInit = [10000*nbJour]*2
        case 2:
            BudgetPartiInit = [int(input('Valeur du budget : '))]*2
            Budget = 10000

    print("Stratégies :\n\t1 : aléatoire\n\t2 : tétu\n\t3 : stochastique expert\n\t4 : meilleur réponse\n\t5 : fictitious play")
    stratEqu1=strat.strats[(int(input("Choix stratégies équipe 1 : "))-1)%5]
    stratEqu2=strat.strats[(int(input("Choix stratégies équipe 2 : "))-1)%5]

    nbSimu = int(input("Nombre de simulation : "))

    init()
    

    
    #-------------------------------
    # Initialisation
    #-------------------------------
    
    nbLignes = game.spriteBuilder.rowsize
    nbCols = game.spriteBuilder.colsize
       
    print("lignes", nbLignes)
    print("colonnes", nbCols)
    
    
    players = [o for o in game.layers['joueur']]
    nbPlayers = len(players)
    print("Trouvé ", nbPlayers, " militants")
    
       
           
    # on localise tous les états initiaux (loc du joueur)
    # positions initiales des joueurs
    initStates = [o.get_rowcol() for o in players]
    print ("Init states:", initStates)
    
    # on localise tous les secteurs d'interet (les votants)
    # sur le layer ramassable
    rama = [o for o in game.layers['ramassable']]
    goalStates = [o.get_rowcol() for o in rama]
    print ("Goal states:", goalStates)
    
        
    # on localise tous les murs
    # sur le layer obstacle
    wallStates = [w.get_rowcol() for w in game.layers['obstacle']]
    print ("Wall states:", wallStates)
    
    def legal_position(row,col):
        # une position legale est dans la carte et pas sur un mur
        return ((row,col) not in wallStates) and row>=0 and row<nbLignes and col>=0 and col<nbCols
    
    
    
        
    #-------------------------------
    # Attributaion aleatoire des fioles 
    #-------------------------------
    
    #-------------------------------
    # Carte demo 
    # 2 joueurs 
    # Joueur 0: A*
    # Joueur 1: random walk
    #-------------------------------
    
    #-------------------------------
    # calcul A* pour le joueur 0
    #-------------------------------
    
    g =np.ones((nbLignes,nbCols),dtype=bool)  # par defaut la matrice comprend des True  
    for w in wallStates:            # putting False for walls
        g[w]=False


    equipe1=[x for x in players if x.get_rowcol()[1]==9]
    equipe2=[x for x in players if x.get_rowcol()[1]==10]
    equipe=(equipe1,equipe2)
    initStates = [o.get_rowcol() for o in equipe1]+[o.get_rowcol() for o in equipe2]
    
    
    #-------------------------------
    # Boucle principale de déplacements 
    #-------------------------------
    
    scoreAll = []

    for _ in range(nbSimu):
        BudgetParti = BudgetPartiInit.copy()
        
        posPlayers = initStates.copy()

        testfin=[0]*nbPlayers

        res=[0,0]
        
        stratE1=[]
        stratE2=[]

        nbObjectif = len(goalStates)
        goalStatesP = [(1,1),(1,8),(3,18),(5,1),(5,18),(14,1),(14,18),(16,18),(18,5),(18,1)]

        a=set()
        for i in range((nbPlayers//2)**(nbObjectif+1)):
            test=[0]*nbObjectif
            for _ in range(nbPlayers//2):
                test[i%nbObjectif]+=1
                i//=nbObjectif
            a.add(tuple(test))

        apE1={x:0 for x in a}
        apE2={x:0 for x in a}

        for jo in range(nbJour):
            random.shuffle(goalStatesP)
            goalStates = goalStatesP[:nbObjectif]
            for i in range(nbObjectif):
                rama[i].set_rowcol(*goalStates[i])

            stratE1,stratE2 = stratEqu1(nbPlayers//2,nbObjectif,stratE1,stratE2,apE1),stratEqu2(nbPlayers//2,nbObjectif,stratE2,stratE1,apE2)

            objectifsE1 = strat.objstrat(stratE1,goalStates)
            objectifsE2 = strat.objstrat(stratE2,goalStates)

            objectifs=(objectifsE1,objectifsE2)
            
            path=[]
            for i in range(nbPlayers):
                p = ProblemeGrid2D(posPlayers[i],objectifs[i//(nbPlayers//2)][i%(nbPlayers//2)],g,'manhattan')
                path += [probleme.astar(p)]
                print (f"Chemin n{i} trouvé:", path[i])

            for i in range(Budget):
                
            # on fait bouger chaque joueur séquentiellement
            
            # Joeur 0: suit son chemin trouve avec A* 

                for j in range(nbPlayers):
                    if BudgetParti[j//(nbPlayers//2)]>0:
                        BudgetParti[j//(nbPlayers//2)]-=1+testfin[j]
                        testfin[j] = aEtoile(j,path,i,posPlayers,equipe[j//(nbPlayers//2)],objectifs[j//(nbPlayers//2)])
                    else:
                        testfin[j] = 1
            
                if all(testfin):
                    break
                
                
            
            # on passe a l'iteration suivante du jeu
                #game.mainiteration()

            arrE1 = [0]*nbObjectif
            arrE2 = [0]*nbObjectif
            arr=(arrE1,arrE2)

            for i in range(nbPlayers):
                if posPlayers[i]==objectifs[i//(nbPlayers//2)][i%(nbPlayers//2)]:
                    arr[i//(nbPlayers//2)][strat.objplayer(posPlayers[i],goalStates)]+=1

            score=[0,0]

            for i,j in zip(arrE1,arrE2):
                if i>j:
                    score[0]+=1
                if j>i:
                    score[1]+=1

            res[0]+=score[0]
            res[1]+=score[1]
            print(f"SCORE jour {jo+1}:\n- equipe 1: {score[0]}\n- equipe 2: {score[1]}")
        print(f"SCORE final:\n- equipe 1: {res[0]}\n- equipe 2: {res[1]}")
        scoreAll += [res]
    print(f"score E1 (strat:{stratEqu1.__name__}) : {[x[0] for x in scoreAll]}\n{sum([x[0]>x[1] for x in scoreAll])} victoires")
    print(f"score E2 (strat:{stratEqu2.__name__}) : {[x[1] for x in scoreAll]}\n{sum([x[1]>x[0] for x in scoreAll])} victoires")
    pygame.quit()
    
    
    #-------------------------------
    
        
        
    
    
        
    
   

if __name__ == '__main__':
    main()
    


