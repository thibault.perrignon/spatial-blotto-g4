# Rapport de projet

## Groupe
* Thibault Perrignon-Sommet

## Description des choix importants d'implémentation

J'ai choisi de mettre les fonctions correspondant aux stratégies dans un fichier strat.py.
Le nombre de jours, le type de budget et les stratégies sont sélectionnés au lancement du programme.
Les stratégies ont tous les mêmes paramètres pour pouvoir utiliser une liste pour les sélectionner.
Les stratégies sont générées sous la forme d'une liste de répartition puis sont transformées en objectif pour chaque équipes.

## Description des résultats

**Légende** :
- X : *gagne plus que Y*
- X+ : *nombre de victoire assez élevé*
- X++ : *nombre de victoire très élevé*
- X100% : *X gagne tout le temps en ignorant le 1er jour*
- X(n) : *X gagne plus si la campagne dure n jours*

#### Budget illimité :

|            A\B         | Aléatoire | Têtu  | Stochastique expert | Meilleure réponse | Fictitious play |
|:---------------------:|:--------------:|:-------------:|:----:|:----:|:----:|
| **Aléatoire**             |∅|égalité|B++|B++|A+(10), B++(30)|
| **Têtu**                  |égalité|∅|B+|B100%|B100%|
| **Stochastique expert**   |A++|A+|∅|B|égalité(10), B+(30)|
| **Meilleure réponse**     |A++|A100%|A|∅|A100%|
| **Fictitious play**       |B+(10), A++(30)|A100%|égalité(10), A+(30)|B100%|∅|

#### Budget limité par jour (15 déplacements):

|            A\B         | Aléatoire | Têtu | Stochastique expert | Meilleure réponse | Fictitious play |
|:---------------------:|:--------------:|:-------------:|:----:|:----:|:----:|
| **Aléatoire**             |∅|égalité|B+|B|A+(10), A++(30)|
| **Têtu**                  |égalité|∅|B++|B++|B++(10), B100%(30)|
| **Stochastique expert**   |A+|A++|∅|égalité|A++|
| **Meilleure réponse**     |A|A++|égalité|∅|A++|
| **Fictitious play**       |B+(10), B++(30)|A++(10), A100%(30)|B++|B++|∅|


#### Budget limité pour la campagne entière (1000 déplacements pour 10 jours):

|            A\B         | Aléatoire | Têtu | Stochastique expert | Meilleure réponse | Fictitious play |
|:---------------------:|:--------------:|:-------------:|:----:|:----:|:----:|
| **Aléatoire**             |∅|égalité|B+|B+|A+(10), égalité(30)|
| **Têtu**                  |égalité|∅|B+|B100%|B100%|
| **Stochastique expert**   |A+|A+|∅|A|A+|
| **Meilleure réponse**     |A+|A100%|B|∅|A++|
| **Fictitious play**       |A+(10), égalité(30)|A100%|B+|B++|∅|