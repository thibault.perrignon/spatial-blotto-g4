# -*- coding: utf-8 -*-

import random

def alea(nbPlayers,nbObj,*_):
    objectifs=[0]*nbObj
    for _ in range(nbPlayers):
        objectifs[random.randint(0,nbObj-1)]+=1
    return tuple(objectifs)

def tetu(nbPlayers,nbObj,stratPrec,*_):
    if len(stratPrec)==0:
        return alea(nbPlayers,nbObj)
    return tuple(stratPrec)

def stoExp(nbPlayers,nbObj,_0,_1,aprio):
    if not any(aprio.values()):
        la=len(aprio)
        for s1 in aprio.copy():
            win=0
            for s2 in aprio.copy():
                score=0
                for i in range(nbObj):
                    if s1[i]>s2[i]:
                        score+=1
                if score>nbObj//2:
                    win+=1
            aprio[s1]=win
        for i in aprio.copy():
            if aprio[i]<la*3//10:
                aprio.pop(i)

    n=random.randint(1,sum(aprio.values()))
    for i in aprio:
        if n-aprio[i]>0:
            n-=aprio[i]
        else:
            return i
    return tuple([2,2,1,1,1]) #valeur par default, ne devrait pas arrivée

def meilRep(nbPlayers,nbObj,_0,stratAdv,*_):
    if len(stratAdv)==0:
        return alea(nbPlayers,nbObj)
    
    hrep=[]
    for i in range(len(stratAdv)):
        hrep+=[stratAdv[i]+1]
        d=sum(hrep)-nbPlayers

    while d > 0:
        i=hrep.index(max(hrep))
        if hrep[i]>=d:
            hrep[i]-=d
            d=0
        else:
            d-=hrep[i]
            hrep[i]=0
    return tuple(hrep)

def fictPlay(nbPlayers,nbObj,_,stratAdv,aprio):
    if len(stratAdv)==0:
        stratAdv = alea(nbPlayers,nbObj)

    if stratAdv not in aprio:
        aprio[stratAdv] = 0
    
    aprio[stratAdv] += 1

    tot = sum(aprio.values())

    proba = []
    for i in aprio:
        proba += [aprio[i]/tot]

    gainMax = 0

    for maStrat in aprio:

        gain = 0

        for p,a in zip(proba,aprio):

            score = 0
            for i,j in zip(maStrat,a):
                if i>j:
                    score += 1

            gain += p*score

        if gain >= gainMax:
            gainMax = gain
            stratMax = maStrat

    return tuple(stratMax)

strats = [alea,tetu,stoExp,meilRep,fictPlay]

def objstrat(strat,objectifs):
    obj=[]
    for i in range(len(strat)):
        obj+=[objectifs[i]]*strat[i]
    return obj

def objplayer(cord,objectif):
    for i in range(len(objectif)):
        if cord == objectif[i]:
            return i
    